import math
import csv
import numpy
import matplotlib.pyplot


linecount = 1
# puts data in file into a list
#90,10,5.1544366,4.9589787,16,-255
#data in form of:
# ActualDegrees, ActualRadius, estimatedx, estimatedy, differencetoneighbors, Sum of Read RSSI
with open("FieldData.txt") as Datafile:
    for line in Datafile:
        datalist= csv.reader(Datafile)
        datalist = list(datalist)

RSSIMagHolder = []
ErrorHolder = []
Group_5 = []
Group_4 = []
Group_3 = []
Group_2 = []
Group_1 = []

Group_5_rooted = []
Group_4_rooted = []
Group_3_rooted = []
Group_2_rooted = []
Group_1_rooted = []

RSSIMagHolder_Group_5 = []
RSSIMagHolder_Group_4 = []
RSSIMagHolder_Group_3 = []
RSSIMagHolder_Group_2 = []
RSSIMagHolder_Group_1 = []

w, h = 1000, len(datalist);
#MagHold = [[0 for x in range(w)] for y in range(h)]
MagHold = [[] for i in range(3000)]
MagIndex = [0 for x in range(w)]
print (len(datalist))

#finding min and max RSSI totals we see in data set
maxval = 0
minval = 300
for n in range(0,len(datalist)):
    RSSIMag = int(abs(float(datalist[n][5])))
    if RSSIMag > maxval:
        maxval = RSSIMag
    if RSSIMag < minval:
        minval = RSSIMag
GroupIncrement = maxval - minval
GroupIncrement = int(GroupIncrement/5)
# Used to split the group into 5 equal parts

# this loop finds the statistics for our data
for n in range(0,len(datalist)):
    actualDeg = float(datalist[n][0])
    actualRad = float(datalist[n][1])
    estimatex = float(datalist[n][2])
    estimatey= float(datalist[n][3])
    RSSIMag = int(abs(float(datalist[n][5])))
    actualy = actualRad*math.sin(math.radians(actualDeg))
    actualx = actualRad * math.cos(math.radians(actualDeg))
    ErrorDistancesqrt = math.sqrt((actualy - estimatey)**2 + (actualx - estimatex)**2)
    ErrorDistance = (actualy - estimatey) ** 2 + (actualx - estimatex) ** 2
    ErrorHolder.append(ErrorDistance)
    MagHold[RSSIMag].append(ErrorDistance)
    RSSIMagHolder.append(RSSIMag)

    #seperates data into the different groups
    if RSSIMag <= minval+GroupIncrement:
        Group_5.append(ErrorDistance)
        Group_5_rooted.append(numpy.sqrt(ErrorDistance))
        RSSIMagHolder_Group_5.append(RSSIMag)
        matplotlib.pyplot.plot(RSSIMag, ErrorDistancesqrt, 'go')
    elif RSSIMag <= minval+2*GroupIncrement:
        Group_4.append(ErrorDistance)
        Group_4_rooted.append(numpy.sqrt(ErrorDistance))
        RSSIMagHolder_Group_4.append(RSSIMag)
        matplotlib.pyplot.plot(RSSIMag, ErrorDistancesqrt, 'co')
    elif RSSIMag <= minval+3*GroupIncrement:
        Group_3.append(ErrorDistance)
        Group_3_rooted.append(numpy.sqrt(ErrorDistance))
        RSSIMagHolder_Group_3.append(RSSIMag)
        matplotlib.pyplot.plot(RSSIMag, ErrorDistancesqrt, 'bo')
    elif RSSIMag <= minval+4*GroupIncrement:
        Group_2.append(ErrorDistance)
        Group_2_rooted.append(numpy.sqrt(ErrorDistance))
        RSSIMagHolder_Group_2.append(RSSIMag)
        matplotlib.pyplot.plot(RSSIMag, ErrorDistancesqrt, 'mo')
    else:
        Group_1.append(ErrorDistance)
        Group_1_rooted.append(numpy.sqrt(ErrorDistance))
        RSSIMagHolder_Group_1.append(RSSIMag)
        matplotlib.pyplot.plot(RSSIMag, ErrorDistancesqrt, 'ro')
print("Mean: " + str(numpy.mean(ErrorHolder)))
print("std dev: " + str(numpy.std(ErrorHolder)))
print("Variance: " + str(numpy.std(ErrorHolder)**2))

def ourfit(x):
    return numpy.sqrt((-0.0002869 * (numpy.power(x, 3)))+
                      (0.1953 * (numpy.power(x, 2))) + (-43.44* numpy.power(x, 1)) + 3174)

print("Group_1(red) mean:" + str(math.sqrt(numpy.mean(Group_1))))
print("Group_2(magenta) mean:" + str(math.sqrt(numpy.mean(Group_2))))
print("Group_3(blue) mean:" + str(math.sqrt(numpy.mean(Group_3))))
print("Group_4(cyan) mean:" + str(math.sqrt(numpy.mean(Group_4))))
print("Group_5(green) mean:" + str(math.sqrt(numpy.mean(Group_5)))+"\n")

#adjusting the slope for each group so it can encompass the proper number of units
# groupX and Y are the data in this group
#groupRMS is from above
#our current slope
def adjustslope (slope, offset, groupX, groupY):
    coverage = 0
    newoff = offset
    newSlope = slope
    while coverage < .68:
        coveragecount = 0
        for count in range(0,len(groupX)):
            if (numpy.sqrt(groupY[count])) < ((groupX[count])*newSlope + newoff):
                coveragecount = coveragecount + 1
        coverage = float(coveragecount/len(groupX))
        newoff = newoff + 0.01
    return (newSlope,newoff)
Group_1_slope , Group_1_offset = numpy.polyfit(RSSIMagHolder_Group_1, Group_1_rooted, 1)
Group_1_slope , Group_1_offset= adjustslope(0 , 0 , RSSIMagHolder_Group_1 , Group_1)
matplotlib.pyplot.plot([minval+GroupIncrement*4, maxval]
                       , [(Group_1_slope*(minval+GroupIncrement*4)+Group_1_offset)
                           , (Group_1_slope*(maxval)+Group_1_offset)], 'k-', lw=3)
print("if RSSI SUM >= " + str(minval+GroupIncrement*4) + "\n" + str(Group_1_slope) +"x + " + str(Group_1_offset) + "\n")

Group_2_slope , Group_2_offset = numpy.polyfit(RSSIMagHolder_Group_2, Group_2_rooted, 1)
Group_2_slope , Group_2_offset= adjustslope(Group_2_slope , Group_2_offset , RSSIMagHolder_Group_2 , Group_2)
matplotlib.pyplot.plot([minval+GroupIncrement*3, minval+GroupIncrement*4]
                       , [(Group_2_slope*(minval+GroupIncrement*3)+Group_2_offset)
                           , (Group_2_slope*(minval+GroupIncrement*4)+Group_2_offset)], 'k-', lw=3)
print("if "+ str(minval+GroupIncrement*3)+" <= RSSI SUM < " + str(minval+GroupIncrement*4) + "\n"
      + str(Group_2_slope) +"x + " + str(Group_2_offset) + "\n")

Group_3_slope , Group_3_offset = numpy.polyfit(RSSIMagHolder_Group_3, Group_3_rooted, 1)
Group_3_slope , Group_3_offset= adjustslope(Group_3_slope , Group_3_offset , RSSIMagHolder_Group_3 , Group_3)
matplotlib.pyplot.plot([minval+GroupIncrement*2, minval+GroupIncrement*3]
                       , [(Group_3_slope*(minval+GroupIncrement*2)+Group_3_offset)
                           , (Group_3_slope*(minval+GroupIncrement*3)+Group_3_offset)], 'k-', lw=3)
print("if "+ str(minval+GroupIncrement*2)+" <= RSSI SUM < " + str(minval+GroupIncrement*3) + "\n"
      + str(Group_3_slope) +"x + " + str(Group_3_offset) + "\n")


Group_4_slope , Group_4_offset = numpy.polyfit(RSSIMagHolder_Group_4, Group_4_rooted, 1)
Group_4_slope , Group_4_offset= adjustslope(Group_4_slope , Group_4_offset , RSSIMagHolder_Group_4 , Group_4)
matplotlib.pyplot.plot([minval+GroupIncrement, minval+GroupIncrement*2]
                       , [(Group_4_slope*(minval+GroupIncrement)+Group_4_offset)
                           , (Group_4_slope*(minval+GroupIncrement*2)+Group_4_offset)], 'k-', lw=3)
print("if "+ str(minval+GroupIncrement)+" <= RSSI SUM < " + str(minval+GroupIncrement*2) + "\n"
      + str(Group_4_slope) +"x + " + str(Group_4_offset) + "\n")

Group_5_slope , Group_5_offset = numpy.polyfit(RSSIMagHolder_Group_5, Group_5_rooted, 1)
Group_5_slope , Group_5_offset= adjustslope(Group_5_slope , Group_5_offset , RSSIMagHolder_Group_5 , Group_5)
matplotlib.pyplot.plot([minval, minval+GroupIncrement]
                       , [(Group_5_slope*(minval)+Group_5_offset)
                           , (Group_5_slope*(minval+GroupIncrement)+Group_5_offset)], 'k-', lw=3)
print("if "+ str(minval)+" <= RSSI SUM < " + str(minval+GroupIncrement) + "\n"
      + str(Group_5_slope) +"x + " + str(Group_5_offset) + "\n")


#matplotlib.pyplot.plot(numpy.unique(RSSIMagHolder),ourfit(numpy.unique(RSSIMagHolder)),'-r',lw=2)

# gives percentage of group inside our predicted linear sigmas
Group_1_count = 0
Group_2_count = 0
Group_3_count = 0
Group_4_count = 0
Group_5_count = 0
for bcount in range(0,len(Group_1)-1):
    if numpy.sqrt(Group_1[bcount]) <= (Group_1_slope * (RSSIMagHolder_Group_1[bcount])) + Group_1_offset:
        Group_1_count += 1
for bcount in range(0,len(Group_2)-1):
    if numpy.sqrt(Group_2[bcount]) <= (Group_2_slope*(RSSIMagHolder_Group_2[bcount])) + Group_2_offset:
        Group_2_count += 1
for bcount in range(0,len(Group_3)-1):
    if numpy.sqrt(Group_3[bcount]) <= (Group_3_slope*(RSSIMagHolder_Group_3[bcount])) + Group_3_offset:
        Group_3_count += 1
for bcount in range(0,len(Group_4)-1):
    if numpy.sqrt(Group_4[bcount]) <= (Group_4_slope * (RSSIMagHolder_Group_4[bcount])) + Group_4_offset:
        Group_4_count += 1
for bcount in range(0,len(Group_5)-1):
    if numpy.sqrt(Group_5[bcount]) <= (Group_5_slope * (RSSIMagHolder_Group_5[bcount])) + Group_5_offset:
        Group_5_count += 1

print("Group_1(red) members:"+str(len(Group_1))+"  Below Sigma: " + str(Group_1_count*100/len(Group_1)) +"%")
print("Group_2(magenta) members:"+str(len(Group_2))+"  Below Sigma: " + str(Group_2_count*100/len(Group_2)) +"%")
print("Group_3(blue) members:"+str(len(Group_3))+"  Below Sigma: " + str(Group_3_count*100/len(Group_3)) +"%")
print("Group_4(cyan) members:"+str(len(Group_4))+"  Below Sigma: " + str(Group_4_count*100/len(Group_4)) +"%")
print("Group_5(green) members:"+str(len(Group_5))+"  Below Sigma: " + str(Group_5_count*100/len(Group_5)) +"%")
matplotlib.pyplot.ylabel('Error(m)')
matplotlib.pyplot.xlabel('Sum of Read RSSI Absolute Value(dBm)')
matplotlib.pyplot.show()